package py.com.qode.ejb;

import java.util.List;


import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;
import javax.transaction.Transactional;

import py.com.qode.modelos.Usuario;


@ApplicationScoped
public class UsuarioDAO {

	@PersistenceContext(unitName = "facturacionPU")
	private EntityManager em;

	protected EntityManager getEm() {
		return em;
	}


	public  boolean validar(String userName, String password) {

		Usuario usuario = this.findByUserName(userName);

		//return  (usuario!=null && BCryptStatic.verify(password, usuario.getPassword()));

		return true; //TODO
	}


	@Transactional
	public Usuario actualizar(Usuario usuario){

		Usuario usuarioEncontrado = this.findByUserName(usuario.getUserName());

		if(usuarioEncontrado == null) {

			//usuario.setPassword(BCryptStatic.hash(usuario.getPassword()));

			em.persist(usuario);

		}else {

			usuario.setUsuarioId(usuarioEncontrado.getUsuarioId());

			//usuario.setPassword(BCryptStatic.hash(usuario.getPassword()));

			em.merge(usuario);

		}

		return usuario;
	}

	public List<Usuario> getAll(){
	    return getEm().createQuery("SELECT p FROM Usuario p", Usuario.class).getResultList();
    }

    @Transactional
    public void eliminar(Usuario usuario){
		em.remove(usuario);
	}

	public Usuario findById(Long id){
		return em.find(Usuario.class, id);
	}

	public Usuario findByUserName(String username){

		return getEm().createQuery("SELECT p FROM Usuario p where p.userName =:user", Usuario.class)
				.setParameter("user", username).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
	}
	@SuppressWarnings("unchecked")
	public Usuario validate(String userName, String password) {
		String consult = "select * from usuario where user_name = '" +userName+ "' and password = '"+password+"';";
		Query q = getEm().createQuery(consult);
		return (Usuario) q.getResultList();
	}
	
}
