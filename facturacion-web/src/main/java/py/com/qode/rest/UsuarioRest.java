package py.com.qode.rest;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import py.com.qode.business.UsuarioBusiness;
import py.com.qode.dto.UsuarioDto;

import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/usuario")
@Api(tags = "/usuario")
@Startup
@Singleton
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioRest {

    @Inject
    UsuarioBusiness usuarioBusiness;

    @Inject
    Logger logger;


    @POST
    @Path("/upsert")
    @ApiOperation(value = "/upsert",response = UsuarioDto.class,
            responseContainer = "List" ,notes = "update or create user")
    @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid ID supplied"),
            @ApiResponse(code = 404, message = "Not found") })
    public Response createUpate(@ApiParam(value = "UsuarioDTO", required = true) @Valid UsuarioDto usuarioDto){

        try {

            return Response.ok(usuarioBusiness.createUpdate(usuarioDto)).build();

        }catch (Exception e){

            logger.error("Error createUpate: ", e);

        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    }

}
