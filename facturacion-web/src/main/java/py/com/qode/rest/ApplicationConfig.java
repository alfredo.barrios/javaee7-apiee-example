package py.com.qode.rest;

import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Willian Martinez <willian.martinez@qode.com.py>
 *
 */
@ApplicationPath("/")
@SwaggerDefinition(
        info = @Info(
                title = "kyrey", version = "1.0", contact = @Contact(
                name = "Rain Moore",
                email = "rain.moore@theweatherapi.io",
                url = "http://theweatherapi.io"
        ))
)
public class ApplicationConfig extends Application {
}
