package py.com.qode.rest;

import java.io.IOException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

@Provider
@PreMatching
public class Intercepter implements ContainerRequestFilter {
	
	private final String HEADER = "Authorization";
	private final String PREFIX = "Bearer ";
	private final String KEY = "clave_facturacion";

	public void filter(ContainerRequestContext request) throws IOException {
		String url = request.getUriInfo().getAbsolutePath().toString();
		String method = request.getMethod();
		if(method.equals(javax.ws.rs.HttpMethod.OPTIONS)) return;
		if(url.contains("/facturacion/api/webresources/auth"))
			return;
		if(url.contains("/facturacion/api/webresources/apiee"))
			return;
		if(url.contains("/facturacion/api/webresources/webjars"))
			return;
		String token = request.getHeaderString(HEADER);
		if(token == null) {
			JsonObject json = Json.createObjectBuilder().add("mensaje","credenciales son necesarias").build();
			request.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(json)
					.type(MediaType.APPLICATION_JSON).build());
			return;
		}
		String jwt = resolveToken(token);

		if(!validateToken(jwt)) {
			JsonObject json = Json.createObjectBuilder().add("mensaje","credenciales incorrectas").build();
			request.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(json)
					.type(MediaType.APPLICATION_JSON).build());
			return;
		}
	}

	public String resolveToken(String bearerToken) {
		if (!bearerToken.isEmpty() && bearerToken.startsWith(PREFIX)) {
			String jwt = bearerToken.substring(7, bearerToken.length());
			return jwt;
		}
		return null;
	}

	public boolean validateToken(String authToken) {

		try {
			Jwts.parser().setSigningKey(KEY).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			//LOGGER.info("Invalid JWT signature: " + e.getMessage());
			//LOGGER.debug("Exception " + e.getMessage(), e);
			return false;
		}
	}
}
