package py.com.qode.business;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import py.com.qode.dto.UsuarioDto;
import py.com.qode.ejb.UsuarioDAO;
import py.com.qode.modelos.Usuario;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Stateless
public class UsuarioBusiness {

    @Inject
    UsuarioDAO usuarioDAO;

    @Inject
    Logger logger;

    @Transactional
    public UsuarioDto createUpdate(UsuarioDto usuarioDto){

        ModelMapper modelMapper = new ModelMapper();

        Usuario usuario = modelMapper.map(usuarioDto, Usuario.class);

        return modelMapper.map(usuarioDAO.actualizar(usuario), UsuarioDto.class);


    }

    public boolean validar(String username, String password){
        try {

            return usuarioDAO.validar(username, password);

        }catch (Exception e){
            logger.error("Validar User/Pass Error: ", e);
            throw e;
        }
    }

    public Usuario getById(Long userId){

        return usuarioDAO.findById(userId);

    }

    public List<Usuario> getAll(){

        return usuarioDAO.getAll();

    }

    public Usuario getByUserName(String username){

        return usuarioDAO.findByUserName(username);

    }

    @Transactional
    public void deleteById(Long id){

        Usuario usuario = usuarioDAO.findById(id);

        if(usuario == null) {
            logger.warn("Usuario a eliminar No encontrado: {} ",id);
            return;
        }

        logger.debug("Eliminando usuario: {}", usuario);

        usuarioDAO.eliminar(usuario);

    }
}
