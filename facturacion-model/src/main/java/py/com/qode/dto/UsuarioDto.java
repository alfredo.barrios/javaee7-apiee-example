package py.com.qode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.StringJoiner;

public class UsuarioDto {

    @NotNull
    @Size(min=1, max = 50)
    private String nombre;

    @NotNull
    @Size(min=1, max = 50)
    private String apellido;

    @NotNull
    @Size(min=1, max = 50)
    private String telefono;

    @NotNull
    @Size(min=1, max = 50)
    private String correo;

    @NotNull
    @Size(min=1, max = 50)
    private String userName;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Size(min=1, max = 75)
    private String password;

    @NotNull
    @Size(min=1, max = 1)
    private String activo;

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UsuarioDto.class.getSimpleName() + "[", "]")
                .add("nombre='" + nombre + "'")
                .add("apellido='" + apellido + "'")
                .add("telefono='" + telefono + "'")
                .add("correo='" + correo + "'")
                .add("userName='" + userName + "'")
                .add("password='" + password + "'")
                .toString();
    }
}
