package py.com.qode.modelos;

import org.hibernate.validator.constraints.Email;

import java.io.Serializable;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = -1564546859068687460L;
	
	@Id
	@Column(name = "usuario_id")
	@GeneratedValue(generator = "usuarioSec")
	@SequenceGenerator(name = "usuarioSec", sequenceName = "usuario_usuario_id_seq", allocationSize = 0)
	private Long usuarioId;
	
	@Column(name = "nombre", length = 50)
	private String nombre;
	
	@Column(name = "apellido", length = 50)
	private String apellido;
	
	@Column(name = "telefono", length = 50)
	private String telefono;

	@Email
	@Column(name = "correo", length = 50)
	private String correo;
	
	@Column(name = "user_name", unique = true, length = 50)
	private String userName;
	
	@Column(name = "password", length = 50)
	private String password;

	@Column(name = "activo", length = 1)
	private String activo;

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Usuario.class.getSimpleName() + "[", "]")
				.add("usuarioId=" + usuarioId)
				.add("nombre='" + nombre + "'")
				.add("apellido='" + apellido + "'")
				.add("telefono='" + telefono + "'")
				.add("correo='" + correo + "'")
				.add("userName='" + userName + "'")
				.add("password='" + password + "'")
				.add("activo='" + activo + "'")
				.toString();
	}
}
